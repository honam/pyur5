import numpy as np

import sys
from mbse.models.ur5_dynamics_model import Ur5PendulumDynamicsModel
from mbse.optimizers.sac_based_optimizer import SACOptimizer
import pickle
import jax 
import jax.numpy as jnp

from mbse.utils.replay_buffer import ReplayBuffer, Transition
import time
import cloudpickle
import wandb 
import matplotlib.pyplot as plt
from gym.envs.classic_control.pendulum import angle_normalize
import math
import os 
from pyur5.utils.utils import normalize, denormalize
# wandb.init(project="sac_policy")

import pyur5

package_path = os.path.dirname(pyur5.__file__)

def rollout_random_policy(train_horizon, true_dynamics, data, rng):
    # model = true_dynamics.model
    rng, reset_rng = jax.random.split(rng, 2)
    print("data.transition: ", data['transitions'])
    obs_vec = data['states']
    action_vec = data['u']
    next_obs_vec = data['states_next']
    num_points = obs_vec.shape[0]
    done_vec = data['transitions'].done#jnp.array([False]* num_points).reshape(-1, 1)
    reward_vec = data['transitions'].reward

    transitions = Transition(
        obs=obs_vec,
        action=action_vec,
        reward=reward_vec,
        next_obs=next_obs_vec,
        done=done_vec,
    )

    # reset 
    print("obs_vec of shape {} is {}".format(obs_vec.shape, obs_vec))
    print("reward_vec of shape {} is {}".format(reward_vec.shape, reward_vec))
    print("next_obs_vec of shape {} is {}".format(next_obs_vec.shape, next_obs_vec))
    print("done_vec of shape {} is {}".format(done_vec.shape, done_vec))
    return transitions


def make_plot(use_cos, task_typ, n_horizon, solver, sac_policy):
    '''
    
    '''
    print("start make_plot")
    true_dynamics.model.n_horizon = n_horizon
    
    x = true_dynamics.reset()
    x_test, u_test, cost = true_dynamics.model.forward_traj(x, n_horizon, solver, sac_policy)

    xs = x_test.tolist()
    us = u_test.tolist() 
    if task_typ == 'new':
        if use_cos:
            cos = [x[0] for x in xs]
            sin = [x[1] for x in xs]
            theta  = [math.atan2(sin[i], cos[i]) for i in range(len(cos))]
            theta_dot = [x[2] for x in xs]
            p_ee_x = [x[3] for x in xs]
            # if n_action > 1:
            p_ee_y = [x[4] for x in xs]
            # if n_action > 2:
            #     p_ee_z = [x[5] for x in obss]  

            v_ee_x = [x[5] for x in xs]
            v_ee_y = [x[6] for x in xs]
            action_x = [a[0] for a in us]
            action_y = [a[1] for a in us]

            datas = [cos, sin, theta, theta_dot, p_ee_x, p_ee_y, v_ee_x, v_ee_y, action_x, action_y]
            headers = ["cos", "sin", "theta", "theta_dot", "p_ee_x", "p_ee_y", "v_ee_x","v_ee_y", "u_x", "u_y"]

        else:
            theta = [x[0] for x in xs]
            theta = [angle_normalize(x) for x in theta]
            theta_dot = [x[1] for x in xs]
            p_ee_x = [x[2] for x in xs]
            # if n_action > 1:
            p_ee_y = [x[3] for x in xs]
            # if n_action > 2:
            #     p_ee_z = [x[5] for x in obss]  

            v_ee_x = [x[4] for x in xs]
            v_ee_y = [x[5] for x in xs]

            action_x = [a[0] for a in us]
            action_y = [a[1] for a in us]
            datas = [theta, theta_dot, p_ee_x, p_ee_y, v_ee_x, v_ee_y, action_x, action_y]
            headers = ["theta", "theta_dot", "p_ee_x", "p_ee_y", "v_ee_x", "v_ee_y", 'u_x', 'u_y']
    elif task_typ == 'sim':
        cos = [x[0] for x in xs]
        sin = [x[1] for x in xs]
        theta  = [math.atan2(sin[i], cos[i]) for i in range(len(cos))]
        theta_dot = [x[2] for x in xs]

        action = [a[0] for a in us]
        datas = [cos, sin, theta, theta_dot, action]
        headers = ["cos", "sin", "theta", "theta_dot", "action"]

    fig, axs = plt.subplots(len(headers), 1, figsize=(30, 30))
    fig.suptitle("{}_{}_{}".format(task_typ, n_horizon, solver))
    for i in range(len(headers)):
        axs[i].plot(datas[i])
        axs[i].set_title(headers[i])

    fig_path = os.path.join(os.path.dirname(package_path),'figs','{}_{}_{}_{}.png'.format(task_typ, n_horizon, use_cos, solver) )
    fig.savefig(fig_path)
    log = {"plot_{}".format(n_horizon): wandb.Image(fig), "cost_{}".format(n_horizon): cost}

    return log


def train_sac_policy_optimizer(config, rng):
    sac_kwargs = config.sac_kwargs
    # sac_kwargs['lr_actor'] = config.lr_actor
    # sac_kwargs['lr_critic'] = config.lr_critic
    # sac_kwargs['lr_alpha'] = config.lr_alpha
    # sac_kwargs['discount'] = config.discount
    
    n_horizon = config.n_horizon
    roll_horizon = config.roll_horizon
    train_steps_per_model_update = config.train_steps_per_model_update
    transitions_per_update = config.transitions_per_update
    use_cos = config.use_cos

    train_rng, rng = jax.random .split(rng, 2)

    task_typ = config.task_typ

    # horizon = 5 
    train_summaries = []

    print("action space dimension: ", true_dynamics.model.action_space.shape)
    policy_optimizer = SACOptimizer(
        dynamics_model_list=dynamics_model_list,
        horizon=roll_horizon,
        action_dim=true_dynamics.model.action_space.shape,
        train_steps_per_model_update=train_steps_per_model_update,#50,# TODO: start with 50 and increase this to till ~350x``
        transitions_per_update=transitions_per_update,#8000, # TODO: as much as possible
        sac_kwargs=sac_kwargs,
        reset_actor_params=False,
    )

    print("optimizer initialized")
    # for run in range(5):
    t = time.time()
    train_summary = policy_optimizer.train(
        rng=train_rng,
        buffer=buffer,
    )

    print("time taken for optimization ", time.time() - t)

    sac_path = os.path.join(package_path, "model_file","sac_policy_{}_{}.pkl".format(task_typ, use_cos))

    with open(sac_path, "wb") as f:
        cloudpickle.dump(policy_optimizer, f)
            
    logs = {}
    
    log = make_plot(use_cos, task_typ, n_horizon, "sac", policy_optimizer)
    logs.update(log)
    return train_summaries, logs


def main():
    wandb.init(project="sac_policy")
    _, logs = train_sac_policy_optimizer(wandb.config, rng)
    wandb.log(logs)


if __name__ == '__main__':

    #################### Begin Buffer ####################
    # train_horizons = [1] #[1,2,3,4,5] #[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    # n_models = [5]#[1, 5, 10]
    # n_horizons = [200]#, 300, 400, 500]#[5, 10, 20, 30, 40, 50, 100, 150, 200]
    # roll_horizons = [5]

    task_typ = 'new'
    train_horizon = 1
    use_cos = True
    roll_horizon = 5 # horizon for rolling for simulation buffer
    

    data_path = os.path.join(package_path, "data_pkl", "data_{}_{}.pkl".format(task_typ, use_cos))
    with open(data_path, "rb") as f:
        data = pickle.load(f)

    print("data loaded")
    # print(data)

    # normalize the data
    
    #################### Begin Buffer ####################
    true_dynamics = Ur5PendulumDynamicsModel(use_cos=use_cos, task_typ=task_typ)
    print("true dynamics model initialized")

    dynamics_model_list = [true_dynamics] # learnt dynamics model
    rng = jax.random.PRNGKey(seed=0)

    buffer = ReplayBuffer(
        obs_shape=true_dynamics.model.obs_space.shape,
        action_shape=true_dynamics.model.action_space.shape,
        max_size=1000000,
        normalize=False,
        action_normalize=False,
        learn_deltas=False
    )
    rollout_rng, rng = jax.random.split(rng, 2)
    transitions = rollout_random_policy(train_horizon, true_dynamics, data, rng=rollout_rng)
    buffer.add(transitions)

    print("buffer loaded")
    # print(buffer)

    #################### End Buffer ####################




    sac_kwargs = {
        'discount': 0.99,
        'init_ent_coef': 1.0,
        'lr_actor': 0.0005,
        'weight_decay_actor': 1e-5,
        'lr_critic': 0.0005,
        'weight_decay_critic': 1e-5,
        'lr_alpha': 0.0005,
        'weight_decay_alpha': 0.0,
        'actor_features': [64, 64],
        'critic_features': [256, 256],
        'scale_reward': 1,
        'tune_entropy_coef': True,
        'tau': 0.005,
        'batch_size': 128,
        'train_steps': 1000, 
    }

    sweep_config = {
        'method': 'grid',
        "name": "sac_policy",
        "metric": {"name": "cost_200", "goal": "minimize"},

        "parameters": {
            "sac_kwargs": {
                "values": [sac_kwargs]
            },
            "roll_horizon": {
                "values" : [1, 2, 3, 4, 5]
            },
            "task_typ": {
                "values" : [task_typ]#['new', 'sim']
            },
            "n_horizon": {
                "values" : [200]
            },
            "train_steps_per_model_update": {
                "values" : [50, 100, 150, 200, 250, 300, 250]#[50+5*i for i in range(61)]
            },
            "transitions_per_update": {
                "values" : [8000+1000*i for i in range(10)]
            },
            "use_cos": {
                "values" : [use_cos]
            }
            # # hyperparameter tuning
            # 'lr_actor': {
            #     'distribution': 'uniform',
            #     'min': 1e-5,
            #     'max': 1e-3
            # },
            # 'lr_critic': {
            #     'distribution': 'uniform',
            #     'min': 1e-5,
            #     'max': 1e-3
            # },
            # 'lr_alpha': {
            #     'distribution': 'uniform',
            #     'min': 1e-5,
            #     'max': 1e-3
            # },
            # 'discount': {
            #     'distribution': 'uniform',
            #     'min': 0.9,
            #     'max': 0.999
            # },
            # # cost function tuning
            # 'cost_theta': {
            #     'distribution': 'uniform',
            #     'min': 1e-3,
            #     'max': 1e+3
            # },
            # 'cost_theta_dot': {
            #     'distribution': 'uniform',
            #     'min': 1e-3,
            #     'max': 1e+3
            # },
            # 'cost_u': {
            #     'distribution': 'uniform',
            #     'min': 1e-3,
            #     'max': 1e+2
            # }
            
        }
    }

    # headers = ["theta", "theta_dot", "p_ee", "v_ee"]
        
    # data_test = data['test']
    # x = data_test['x'][:,:-1]
    # u = data_test['u']
    # y = data_test['y']

    sweep_id = wandb.sweep(sweep_config, project="sac_policy")

    print("sweep_id is {}".format(sweep_id))
    wandb.agent(sweep_id, function=main, count=1000)

