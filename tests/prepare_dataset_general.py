from pyur5.utils.utils import get_data_jax, create_metadata
import jax.numpy as jnp
import pickle

import numpy as np
from mbse.utils.replay_buffer import ReplayBuffer, Transition
from pyur5.models.ens_model import EnsembleModel
import jax
import os
import pyur5
from mbse.models.environment_models.pendulum_swing_up import CustomPendulumEnv

def get_angles(states):
    return np.arctan2(states[:, 1], states[:, 0])
task_typs = ['new', 'pole']

task_typ = 'new'
# data_types = ['train_old', 'valid', 'test']

data = dict()
moving_win = None
train_horizon = 1 
use_coss = [True, False]
task_typs = ['new', 'sim']
# fpath = "../../data/recording_robot_pole.txt"
package_path  = os.path.dirname(pyur5.__file__)
_fpaths = ["data/recording_robot_new_bottom_step0.017_v2.txt", "data/recording_robot_new_bottom_step0.017.txt", "data/recording_robot_new_top_step0.017.txt"]
fpaths = [os.path.join(package_path, fpath) for fpath in _fpaths]

task_typ = 'new'
n_action = 2
num_data_points = 100000
n_rollout = 50
n_obs = 3
seed = 43

for task_typ in task_typs:
    for use_cos in use_coss:
        model = EnsembleModel(use_cos=use_cos, task_typ=task_typ)

        if task_typ == 'new':
            for i, fpath in enumerate(fpaths):
                if 'bottom' in fpath:
                    top = False
                else:
                    top = True
                
                states, x, y, u, ts, states_next = get_data_jax(fpath, top=top, n_action=n_action, n_obs=n_obs, use_cos=use_cos)
                print(u.shape)
                if i == 0:
                    data= dict()
                    data['states'] = states
                    data['xu'] = x
                    data['y'] = y
                    data['u'] = u
                    data['t'] = ts
                    data['states_next'] = states_next
                    
                
                else:
                    data['states'] = np.concatenate([data['states'], states])
                    data['xu'] = np.concatenate([data['xu'], x])
                    data['y'] = np.concatenate([data['y'], y])
                    data['u'] = np.concatenate([data['u'], u])
                    data['t'] = np.concatenate([data['t'], ts])
                    data['states_next'] = np.concatenate([data['states_next'], states_next])

                    data['mu_xu'] = data['xu'].mean(0)
                    data['mu_y'] = data['y'].mean(0)
                    data['std_x'] = data['xu'].std(0)
                    data['std_y'] = data['y'].std(0)

        elif task_typ == 'sim':
            if use_cos == False:
                break
            env = CustomPendulumEnv()

            obs_vec = np.zeros((num_data_points, env.observation_space.shape[0]))
            action_vec = np.zeros((num_data_points, env.action_space.shape[0]))
            next_obs_vec = np.zeros((num_data_points, env.observation_space.shape[0]))
            done_vec = np.zeros((num_data_points, 1))
            dobs_vec = np.zeros((num_data_points, env.observation_space.shape[0]))
            reward_vec = np.zeros((num_data_points, 1))

            sample_per_rollout = int(num_data_points/n_rollout)
            ind = 0
            for i in range(n_rollout):
                obs, _ = env.reset_random()
                for j in range(sample_per_rollout):
                    action = env.action_space.sample()
                    next_obs, reward, done, info, _ = env.step(action)
                    obs_vec[ind] = obs
                    action_vec[ind] = action
                    next_obs_vec[ind] = next_obs
                    dobs_vec[ind] = next_obs - obs
                    obs = next_obs
                    ind += 1

            data = dict()
            data['states'] = obs_vec
            data['xu'] = jnp.concatenate([obs_vec, action_vec], axis=1)
            data['y'] = dobs_vec
            data['u'] = action_vec
            data['states_next'] = next_obs_vec

            data['mu_xu'] = data['xu'].mean(0)
            data['mu_y'] = dobs_vec.mean(0)
            data['std_x'] = data['xu'].std(0)
            data['std_y'] = dobs_vec.std(0)

        create_metadata(data, task_typ, use_cos)
        
        model = EnsembleModel(use_cos=use_cos, task_typ=task_typ)

        x = data['xu']
        u = data['u']
        states = data['states']
        states_next = data['states_next']

        if task_typ == 'new':
            done_vec = np.zeros((len(u), 1))    
        print("x: ", x.shape)
        print("u: ", u.shape)
        print("states: ", states.shape)

        reward_fn = jax.vmap(model._reward_fn, in_axes=(0, 0))
        reward_vec = reward_fn(states, u)

        print("reward_vec: ", reward_vec.shape)
        
        data['transitions'] = Transition(
            obs=states,
            action=u,
            reward=reward_vec,
            next_obs=states_next,
            done=done_vec,
        )

        data_path = os.path.join(package_path, "data_pkl/data_{}_{}.pkl".format(task_typ, use_cos))
        with open(data_path, "wb") as f:
            pickle.dump(data, f)



