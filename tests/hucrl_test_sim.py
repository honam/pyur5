import jax
import sys
sys.path.append('/home/honam/workspace/ode/pyur5/include/mbse')

from mbse.agents.model_based.model_based_agent import ModelBasedAgent
from mbse.models.active_learning_model import ActiveLearningHUCRLModel
from mbse.utils.replay_buffer import ReplayBuffer
from gym.wrappers.rescale_action import RescaleAction
from gym.wrappers.time_limit import TimeLimit
from mbse.utils.vec_env.env_util import make_vec_env
from mbse.models.environment_models.pendulum_swing_up import CustomPendulumEnv, PendulumReward
import pickle 
import matplotlib.pyplot as plt

wrapper_cls = lambda x: RescaleAction(
        TimeLimit(x, max_episode_steps=200),
        min_action=-1,
        max_action=1,
    )

env = make_vec_env(env_id=CustomPendulumEnv, wrapper_class=wrapper_cls, n_envs=1, seed=0)

lr = 5e-4
)
dynamics_model = ActiveLearningHUCRLModel(
            action_space=env.action_space,
            observation_space=env.observation_space,
            num_ensemble=5,
            reward_model=reward_model,
            features=[256, 256],
            pred_diff=True,
            beta=1.0,
            seed=0,
            use_log_uncertainties=True,
            use_al_uncertainties=False,
            deterministic=True,
            lr=lr,
)

agent = ModelBasedAgent(
        train_steps=200000,
        batch_size=256,
        max_train_steps=8000,
        num_epochs=50,
        action_space=env.action_space,
        observation_space=env.observation_space,
        dynamics_model=dynamics_model,
        n_particles=10,
        reset_model=True,
        policy_optimizer_name='TraJaxTO',
        horizon=20,
)

buffer = ReplayBuffer(
                 obs_shape=env.observation_space.shape,
                 action_shape=env.action_space.shape,
                 normalize=True,
                 action_normalize=True,
                 learn_deltas=True,
)

with open("../../../data_pkl/sim_data.pkl", "rb") as f:
    data = pickle.load(f)

sim_transitions = data['train']['transitions']

buffer.add(transition=sim_transitions) #add unnormalized data

agent.set_transforms(
                bias_obs=buffer.state_normalizer.mean,
                bias_act=buffer.action_normalizer.mean,
                bias_out=buffer.next_state_normalizer.mean,
                scale_obs=buffer.state_normalizer.std,
                scale_act=buffer.action_normalizer.std,
                scale_out=buffer.next_state_normalizer.std,
)

rng = jax.random.PRNGKey(0)
rng, train_rng = jax.random.split(rng, 2)
obs, _ = env.reset(seed=0)

total_train_steps = agent.train_step(
    rng=train_rng,
    buffer=buffer,
    validate=True,
    log_results=False, # can change if you want to log to wandb
)

test_env = make_vec_env(env_id=CustomPendulumEnv, wrapper_class=wrapper_cls, n_envs=1, seed=0)
                        #env_kwargs={'render_mode': 'human'})
obs, _ = test_env.reset(seed=0)
done = False
steps = 0
idx = 0

obss = []
next_obss = []
actions = []
while not done:
    action = agent.act(obs, rng=rng, eval=True, eval_idx=0)
    next_obs, reward, terminate, truncate, info = env.step(action)
    done = terminate or truncate
    obs = next_obs
    steps += 1
    print("Step: {}, Reward: {}, obs: {}, next_obs: {}".format(steps, reward, obs, next_obs))
    # print(steps)
    obss.append(obs)
    next_obss.append(next_obs)
    actions.append(action)
    
    if done:
        obs, _ = env.reset(seed=0)

train_horizon = 1
n_model = 5
n_horizon = 200
solver = 'ilqr'

cos = [x[0][0] for x in obss]
sin = [x[0][1] for x in obss]
theta_dot = [x[0][2] for x in obss]

datas = [cos, sin, theta_dot]
headers = [ 'cos', 'sin', 'theta_dot']

fig, axs = plt.subplots(4, 1, figsize=(20, 20))
fig.suptitle("{}_{}_{}_{}".format(train_horizon, n_model, n_horizon, solver))
for i in range(obss[0].shape[1]):
    axs[i].plot(datas[i])
    axs[i].set_title(headers[i])
axs[-1].plot(actions)
axs[-1].set_title("u")

fig.savefig("sim_data.png")