import pickle
import numpy as np
from pyur5.models.ens_model import EnsembleModel

model = EnsembleModel()

task_typ = 'new'
action_max = 1.0
train_horizon = 1
use_cos = True
n_model = 10
roll_horizon = 5 # horizon for rolling for simulation buffer

actions = model.load_trajs()
print(actions.shape)
print(actions)