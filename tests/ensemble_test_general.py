import numpy as np
import matplotlib.pyplot as plt

from pyur5.utils.utils import normalize, load_dataset
from mbse.utils.models import ProbabilisticEnsembleModel, FSVGDEnsemble, KDEfWGDEnsemble
import seaborn as sns
sns.reset_defaults()
sns.set_context(context='talk', font_scale=1.0)
from jax.config import config
import wandb 
# config.update('jax_disable_jit', True)
import pickle
import jax.numpy as jnp
import copy
import cloudpickle

from sklearn.model_selection import train_test_split

import os 
import pyur5

package_path = os.path.dirname(pyur5.__file__)

def dataset(x, y, batch_size):
    ids = np.arange(len(x))
    while True:
        ids = np.random.choice(ids, batch_size, False)
        yield x[ids].astype(np.float32), y[ids].astype(np.float32)

batch_size = 256

def train():
    wandb.init(project="ur5_model_train")
    config = wandb.config
    print(config)
    print("start training...")
    lr = config.lr
    n_layers = config.n_layers
    featrues = config.features * n_layers
    num_ensemble = config.num_ensemble

    use_cos = config.use_cos
    task_typ = config.task_typ
    
    if task_typ == 'new':
        action_max = 1.0
    elif task_typ == 'sim':
        action_max = 2.0
    
    data_path = os.path.join(package_path, "data_pkl", "data_{}_{}.pkl".format(task_typ, use_cos))
    print("data_path: ", data_path)
    print("load data...")
    with open(data_path, "rb") as f:
        data = pickle.load(f)
    print("data loaded")

    x, y, x_val, y_val, x_tst, y_true = load_dataset(data, task_typ, use_cos)

    print("x.shape", x.shape)


    data_train = iter(dataset(x, y, batch_size))

    num_train_steps = 2000
    ModelName = "ProbabilisticEnsemble"
    
    print("model built...")
    if ModelName == "ProbabilisticEnsemble":
        model = ProbabilisticEnsembleModel(
            example_input=x[:batch_size],
            features=featrues,#[32],#[256, 256, 256, 256],
            num_ensemble=num_ensemble,#NUM_ENSEMBLES, 
            lr=lr,#0.0005,
            deterministic=True,
            output_dim=y.shape[-1]
        )
        NAME = 'probabilistic_ensemble_'
    elif ModelName == "fSVGD":
        model = FSVGDEnsemble(
            example_input=x[:batch_size],
            features=[64, 64],
            num_ensemble=NUM_ENSEMBLES,
            lr=0.005,
        )
        NAME = 'fsvgd_ensemble_'
    else:
        model = KDEfWGDEnsemble(
            example_input=x[:batch_size],
            features=[64, 64],
            num_ensemble=NUM_ENSEMBLES,
            lr=0.005,
            #prior_bandwidth=100,
        )
        NAME = 'kde_ensemble_'

    val_loss_best = 1e10


    for i in range(num_train_steps):

        train_loss, train_loss_grad = model.train_step(*next(data_train))

        def loss_fn(x, y):
            y_pred = model.predict(x)
            mu, sig = jnp.split(y_pred, 2, axis=-1)
            loss = jnp.mean((mu.mean(0) - y)**2)
            return loss 
        
        val_loss = loss_fn(x_val, y_val)
        tst_loss = loss_fn(x_tst, y_true)
        if val_loss < val_loss_best:
            val_loss_best = val_loss
            tst_loss_best = tst_loss
            model_best = copy.deepcopy(model)
            model_path = os.path.join(package_path, "model_file", "{}_best_model_{}.pkl".format(task_typ, wandb.run.name))
            with open(model_path, "wb") as f:
                cloudpickle.dump(model_best, f)
            count = 0
        else:
            count += 1
        if count > 50:
            break
        log = {"train_loss": train_loss, "train_loss_grad": train_loss_grad, "val_loss": val_loss, "tst_loss": tst_loss, "val_loss_best": val_loss_best, "tst_loss_best": tst_loss_best, "epoch": i+1}
        wandb.log(log)
        
    wandb.finish()

sweep_config = {
    'method': 'grid',
    'metric': {
        'name': 'val_loss_best',
        'goal': 'minimize'
    },
    'parameters': {
        'num_ensemble': {
            'values': [10]#[5, 10, 20]
        },
        'lr': {
            'values': [0.0005, 0.001, 0.005]#[0.0005]#[0.0005, 0.001, 0.005]
        },
        'features': {
            'values': [[64], [128], [256]]#[[256]]#[[32], [64], [128], [256]],#[[256]]#[[32], [64], [128], [256]] #[256]]#,
        },
        'n_layers': {
            'values': [2, 3, 4]#[4]#[1, 2, 3, 4]
        },
        "use_cos": {
            "values": [True]#[True, False]
        },
        'action_max': {
            'values': [1.0]
        },
        'task_typ': {
            'values': ['new']#['new', 'sim']
        }
    }
}

sweep_id = wandb.sweep(sweep_config, project="ur5_model_train")
wandb.agent(sweep_id, function=train)
