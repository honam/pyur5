from pyur5.models.ens_model import EnsembleModel

model = EnsembleModel()
x = model.obs_space.sample()

print(model.get_sac_action(x))