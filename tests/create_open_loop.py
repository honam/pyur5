import pickle
import numpy as np
from pyur5.models.ens_model import EnsembleModel

model = EnsembleModel()

task_typ = 'new'
action_max = 1.0
train_horizon = 1
use_cos = True
n_model = 10
roll_horizon = 5 # horizon for rolling for simulation buffer

with open("/home/bizoffermark/workspace/ode/pyur5/pyur5/data_pkl/data_{}_{}_{}_{}.pkl".format(task_typ, action_max, train_horizon, use_cos), "rb") as f:
    data = pickle.load(f)

x = data['states'][0]
n_horizon = 200
optimizer = 'cem_ilqr'
model.forward_traj_and_save(x, n_horizon, optimizer)
