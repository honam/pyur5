#!/home/bizoffermark/anaconda3/envs/ode/bin/python3

import os
import subprocess
import argparse

cur_dir = os.getcwd()

dir_lists = cur_dir.split('src')

build_dir = os.path.join(cur_dir, 'build')

# TODO: Make it automatic
# replay_dir =  "/home/bizoffermark/workspace/ode/crl-system-logger/src/apps/replayApp" #os.path.join(os.getcwd().split("ur5-pendulum")[0],"crl-system-logger/build/src/")

def run_commands(commands, chdir, curdir):
    os.chdir(chdir)
    for command in commands:
        subprocess.run(command)
    os.chdir(curdir)

if not os.path.exists(build_dir):
    os.mkdir(build_dir)

parser = argparse.ArgumentParser()
parser.add_argument("--build", action="store_true", default=False, help="build the directory")
parser.add_argument("--is_git", action='store_true', default=False, help="build the directory")
parser.add_argument("--branch", default='ode', choices=['ode', 'main'])
parser.add_argument("--git_message", type=str, default="default", help="message for pushing git directory")
parser.add_argument("--run", action="store_true", default=False, help="build the directory")
parser.add_argument("--cmake", action="store_true", default=False, help="cmake the directory")
parser.add_argument("--sim", action="store_true", default=False, help="run with simulation instead of real robot")
parser.add_argument("--all", action="store_true", default=False, help="perform cmake, build and run in sequence")
parser.add_argument("--replay", action="store_true", default=False, help="perform replay")
parser.add_argument("--is_multirobot", action="store_true", default=False, help="use multiple robots")
args = parser.parse_args()

if args.all:
    args.run = True
    args.build = True
    args.cmake = True

with open("CMakeLists.txt", 'r') as f:
    cmakelists_lines = f.readlines()
if args.sim:
    print(cmakelists_lines[-1])
    if cmakelists_lines[-1] == "add_definitions(-DSIM)":
        pass
    else:
        with open("CMakeLists.txt", "w") as f:
            cmakelists_lines.append("add_definitions(-DSIM)")
            with open("CMakeLists.txt", "w") as f:
                for line in cmakelists_lines:
                    f.write(line)
else:
    if cmakelists_lines[-1] == "add_definitions(-DSIM)":
        cmakelists_lines = cmakelists_lines[:-1]
        with open("CMakeLists.txt", "w") as f:
            for line in cmakelists_lines:
                f.write(line)

if args.cmake:
    if os.path.exists('/home/bizoffermark/anaconda3/envs/ode'):
        run_commands([['cmake', '-DPython_EXECUTABLE=/home/bizoffermark/anaconda3/envs/ode/bin/python3.10', '..']], build_dir, cur_dir)
    else:
        run_commands([['cmake', '-DPython_EXECUTABLE=/home/bizoffermark/anaconda3/bin/python3.9', '..']], build_dir, cur_dir)

if args.build:
    # os.chdir(build_dir)
    # subprocess.run(['make'])
    # os.chdir(cur_dir)
    run_commands([['make']], build_dir, cur_dir)


if args.is_git:
    commands = [
        ['git', 'checkout', args.branch],
        ['git', 'add', '.'],
        ['git', 'commit', '-m', args.git_message],
        ['git', 'push']
    ]
    run_commands(commands, cur_dir, cur_dir)

if args.run:
    run_dir = build_dir + '/src'+ dir_lists[-1]
    # os.chdir(run_dir)
    commands = [['./ur5DCartPole']]
    # os.chdir(cur_dir)
    run_commands(commands, run_dir, cur_dir)

if args.replay:
    import shutil

    # Set the source directory and file names
    source_dir = '/home/bizoffermark/workspace/ode/ur5-pendulum/src/demos/ur5CartPole/data/'
    # pendulum_file = 'recording_pendulum.txt'
    robot_file = 'recording_robot_test.txt'

    # Set the destination directory
    dest_dir = '/home/bizoffermark/workspace/ode/crl-system-logger/src/apps/replayApp/recording/'

    # Move the files to the destination directory
    # shutil.copy(source_dir + pendulum_file, dest_dir)
    shutil.copy(source_dir + robot_file, dest_dir)

    for i in range(1, 7):
        robot_file = 'recording_robot_test_' + str(i) + '.txt'
        shutil.copy(source_dir + robot_file, dest_dir)
    commands = [['python3', 'run.py', '--build','--run']]#'./replayApp']]
    run_commands(commands, replay_dir, cur_dir)
