#import jax.numpy as jnp
#from jax import grad, jit, vmap
import json
import numpy as np
import torch
import jax.numpy as jnp
from jax import vmap
import tensorflow as tf
import tensorflow_datasets as tfds
import trajax
from sklearn.model_selection import train_test_split
import os
import pyur5
import pickle

#def load_model(model_path):
#    model = jnp.load(model_path)
#    return model

# read txt json file
def read_json(file_path):
    with open(file_path) as f:
        data = json.load(f)
    return data

def states2ind_states(states):
    '''
        Extract useful states from the states
        
        states: (N, 24)
    '''
    thetas = states[:,12]
    theta_dots = states[:,13]
    p_pivots = states[:,14:17]
    v_ees = states[:,17:20]
    p_balls = states[:,20:23]
    mocap_valids = states[:,23]
    states = states[:,0:12]
    return states, thetas, theta_dots, p_pivots, v_ees, p_balls, mocap_valids

def pytorch_rolling_window(x, window_size, step_size=1):
    # unfold dimension to make our rolling window
    return x.unfold(0,window_size,step_size)

def normalize(x, mu_x, std_x, eps=1e-8):
    return (x - mu_x)/(std_x + eps)

def denormalize(x, mu_x, std_x, eps=1e-8):
    return (std_x + eps)*x + mu_x

def create_data(theta, theta_dot, p_pivot, v_ee, fpath, data_type, j, test=None, pred_horizon=1):
    '''
        Create data in json for running in the simulation
    '''
    with open(fpath, 'r') as f:
        file = f.readlines()
    modified_json_lines = []
    i = 0
    for k, line in enumerate(file):
        # Parse the line into a JSON object
        json_obj = json.loads(line)
        if test is None:
            if json_obj[1]['state'][-1][0] < 0.5:
                print("{}".format(i))
            else:
                #print(i)
                # Modify the object (for example, add a new key-value pair)
                #json_obj[0]['new_key'] = 'new_value'
                # theta
                json_obj[1]['state'][12][0] = theta[i]
                # theta_dot
                json_obj[1]['state'][13][0] = theta_dot[i]
                # p_pivot
                #json_obj[1]['state'][14:17] = [[x] for x in p_pivot[i]]
                json_obj[1]['state'][14][0] =  p_pivot[i]
                # v_ee
                #json_obj[1]['state'][17:20] = [[x] for x in v_ee[i]]
                json_obj[1]['state'][17][0] = v_ee[i]

                # Convert the modified object back into a JSON-formatted string
                modified_json_line = json.dumps(json_obj) + '\n'
                modified_json_lines.append(modified_json_line)
                i += 1
        else:
            json_obj[1]['state'][12][0] = theta[i]
            # theta_dot
            json_obj[1]['state'][13][0] = theta_dot[i]
            # p_pivot
            #json_obj[1]['state'][14:17] = [[x] for x in p_pivot[i]]
            json_obj[1]['state'][14][0] =  p_pivot[i]
            # v_ee
            #json_obj[1]['state'][17:20] = [[x] for x in v_ee[i]]
            json_obj[1]['state'][17][0] = v_ee[i]

            # Convert the modified object back into a JSON-formatted string
            modified_json_line = json.dumps(json_obj) + '\n'
            modified_json_lines.append(modified_json_line)
            i+=1 
            if i == len(theta):
                break
    # Write the modified JSON lines to a new file
    with open("../../data/recording_robot_{}_{}.txt".format(data_type, j), 'w') as f:
        f.writelines(modified_json_lines)

def moving_average(x, window_size):
    """Compute the moving average of a sequence using a sliding window."""
    x = jnp.concatenate((jnp.array([0.0]*(window_size-1)),x))
    weights = jnp.repeat(1.0, window_size) / window_size
    return jnp.convolve(x, weights, mode='valid')


def get_data_jax(fpath, n_action = 2, top = False, n_obs = 3, use_cos = False):
    '''
        retrieve data from json file
    '''
    train_horizon = 1 
    # TODO: Moving window is not applied at all..
    ts = []
    states = []
    actions = []
    with open(fpath, "r") as f:
        for line in f.readlines():
            data =json.loads(line)
            ts.append(data[0]["t"])
            states.append(data[1]['state'])
            actions.append(data[2]['input'])
    ts = np.array(ts)
    states = np.array(states)
    actions = np.array(actions)
    states = states[:,:,0]
    actions = actions[:,:n_action,0]

    # TODO: Change the following name for states because states here actually mean joint states, which are irrelevant for us!
    states, thetas, theta_dots, p_pivots, v_ees, p_balls, mocap_valids = states2ind_states(states)
    mocap_valids = np.logical_and(mocap_valids[train_horizon:] == True, mocap_valids[:-train_horizon] == True)
    mocap_valids = np.concatenate([mocap_valids, np.array([False])])
    
    # create index for the valid mocap data
    if top:
        idx1 = np.all(actions != 0, axis=1)
        idx_now = np.logical_and(idx1, mocap_valids)
    else:
        idx_now = mocap_valids


    idx_next = np.roll(idx_now, train_horizon)
    for i in range(train_horizon):
        idx_next[i] = False
    
    #TODO: Change the following so that state dimension expansion workms for 1D p_pivots
    if use_cos:
        states_total = [np.expand_dims(np.cos(thetas), -1), np.expand_dims(np.sin(thetas), -1), np.expand_dims(theta_dots, -1), p_pivots[:,:n_obs], v_ees[:,:n_obs]]
    else:
        states_total = [np.expand_dims(thetas,-1), np.expand_dims(theta_dots, -1), p_pivots[:,:n_obs], v_ees[:,:n_obs]]

    # reshape tensors
    states_total = np.concatenate(states_total, 1)

    if n_action == 1:
        actions = np.expand_dims(actions,-1)

    states = states_total[idx_now]
    states_next = states_total[idx_next]

    print("states.shape: ", states.shape)
    us = actions[idx_now]
    ys = states_total[idx_next] - states_total[idx_now]

    print("ys.norm.shape: ", np.linalg.norm(ys, axis=1).shape)
    # get the norm of each row
    aug_ind = np.logical_and(np.all(us == 0, axis=1), np.all( np.expand_dims(np.linalg.norm(ys, axis=1), -1) < 0.001, axis=1))

    print("total aug_ind: ", aug_ind.sum())
    # replicate 2000 copies of xs[aug_ind], ys[aug_ind], us[aug_ind] and ts[aug_ind] and concatenate them to the original data
    # do it smarter way
    n_aug = 2000

    print("us augmented: ", us[aug_ind])
    print("ys augmented: ", ys[aug_ind])
    print("states augmented: ", states[aug_ind])

    us = np.concatenate([us] + [us[aug_ind]]*n_aug, 0)
    ys = np.concatenate([ys] + [ys[aug_ind]]*n_aug, 0)
    states_next = np.concatenate([states_next] + [states[aug_ind]]*n_aug, 0)
    states = np.concatenate([states] + [states[aug_ind]]*n_aug, 0)
    # states_total = np.concatenate([states_total] + [states_total[idx_now][aug_ind]]*n_aug, 0)

    if not use_cos:
        ys[:,0] = np.arctan2(np.sin(ys[:,0]), np.cos(ys[:,0]))

    xs = np.concatenate([states, us], -1)
    print("xs.shape after concat: ", xs.shape)
    print("xs.shape: ", xs.shape)
    print("ys.shape: ", ys.shape)
    print("us.shape: ", us.shape)

    return states, xs, ys, us, ts, states_next

# def get_data(fpath, n_horizon=1):
#     ts = []
#     states = []
#     actions = []
#     with open(fpath, "r") as f:
#         for line in f.readlines():
#             data =json.loads(line)
#             ts.append(data[0]["t"])
#             states.append(data[1]['state'])
#             actions.append(data[2]['input'])

#     ts = np.array(ts)
#     states = np.array(states)
#     actions = np.array(actions)


#     #states, thetas, theta_dots, p_pivots, v_ees, p_balls, mocap_valids = states2ind_states(states)
#     states = states.squeeze(-1)
#     actions = actions[:,0,0]
#     states, thetas, theta_dots, p_pivots, v_ees, p_balls, mocap_valids = states2ind_states(states)
#     #states = [thetas.reshape(thetas.shape[0],-1), theta_dots.reshape(theta_dots.shape[0],-1), p_pivots[:,0], v_ees[:,0]]
#     states = [thetas, theta_dots, p_pivots[:,0], v_ees[:,0]]
#     states = [torch.from_numpy(state).unsqueeze(-1) for state in states]
#     states = torch.concat(states,1)
#     actions= torch.from_numpy(actions).unsqueeze(-1)
#     ts = torch.from_numpy(ts).unsqueeze(-1)
    
#     states = states[mocap_valids>0]
#     actions = actions[mocap_valids>0]
#     ts = ts[mocap_valids>0]
#     x = states[:-1]
#     y = states[1:] - states[:-1]
#     u = actions[:-1]
#     x = torch.cat([x,u], 1)
#     u = actions
#     print((mocap_valids>0).sum())
#     return states, x, y, u, ts

# def make_dataset(data, data_types, batch_size):
#     shuffles = [True, False, False]
#     # infinite = True

#     datasets = []
#     for i , data_type in enumerate(data_types):
        
#         xs_norm = data[data_type]['x_norm']
#         ys_norm = data[data_type]['y_norm']
        
#         ds = tf.data.Dataset.from_tensor_slices((xs_norm, ys_norm))
#         if shuffles[i]:
#             seed = 0
#             ds = ds.shuffle(buffer_size=4 * batch_size, seed=seed, reshuffle_each_iteration=True)
#         # if infinite:
#         #     ds = ds.repeat()
#         ds = ds.batch(batch_size)
#         ds = tfds.as_numpy(ds)
#         datasets.append(ds)
#     return datasets


def dynamics_fn(state, action, model, mu_y, std_y):
    xu = jnp.concatenate([state, action], 1)
    dx = model(xu)
    dx = denormalize(dx, mu_y, std_y)
    return state + dx

def cost_fn(state, action, qs, target_state):
    theta = state[0]
    theta_dot = state[1]
    # p_pivot = state[2]
    # v_ee = state[3]
    
    q_theta = qs[0]
    q_theta_dot = qs[1]
    q_u = qs[2]
    
    theta_star = target_state[0]
    theta_dot_star = target_state[1]
    
    L = q_theta * (theta - theta_star)**2 + \
        q_theta_dot * (theta_dot - theta_dot_star)**2 + \
        q_u * action**2 
    
    return L

    
def objective(cost, dynamics, initial_state, actions, qs, target_state):
    state = initial_state
    n_horizon = actions.shape[0]
    L = 0.0
    for t in range(n_horizon):
        loss = cost(state, actions[t], qs, target_state)
        state = dynamics(state, actions[t])
        L += loss
    return L    

# def improve_controls(cost, dynamics, U, x0, eta, n_iters):
#     grad_fn = jax.grad(trajax.objective, argnums=(4,))
#     for i in range(n_iters):
#         U = U - eta * grad_fn(cost, dynamics, x0, U)
#     return U 


def create_metadata(data, task_typ, use_cos, seed=43):
    np.random.seed(seed)

    x_total = data['xu']
    y_total = data['y']

    # shuffle data
    idx = np.arange(len(x_total))
    np.random.shuffle(idx)
    x_total = x_total[idx]
    y_total = y_total[idx]
    
    x, x_tst, y, y_true = train_test_split(x_total, y_total, test_size=0.2, train_size=0.8, random_state=41)
    x, x_val, y, y_val = train_test_split(x, y, test_size = 0.25, train_size =0.75, random_state=42)

    mu_x = np.mean(x, axis=0)
    std_x = np.std(x, axis=0)
    mu_y = np.mean(y, axis=0)
    std_y = np.std(y, axis=0)
    package_path  = os.path.dirname(pyur5.__file__)

    metadata_path = os.path.join(package_path, "metadata/metadata_{}_{}.pkl".format(task_typ, use_cos))
    print("metadata_path: ", metadata_path)

    print("build metadata...")
    metadata = dict()
    metadata['mu_xu'] = mu_x
    metadata['std_xu'] = std_x
    metadata['mu_y'] = mu_y
    metadata['std_y'] = std_y

    metadata['min_xu'] = x.min(axis=0)
    metadata['max_xu'] = x.max(axis=0)
    with open(metadata_path, "wb") as f:
        pickle.dump(metadata, f)
    
def load_dataset(data, task_typ, use_cos, seed=43):
    np.random.seed(seed)

    x_total = data['xu']
    y_total = data['y']

    # shuffle data
    idx = np.arange(len(x_total))
    np.random.shuffle(idx)
    x_total = x_total[idx]
    y_total = y_total[idx]
    
    x, x_tst, y, y_true = train_test_split(x_total, y_total, test_size=0.2, train_size=0.8, random_state=41)
    x, x_val, y, y_val = train_test_split(x, y, test_size = 0.25, train_size =0.75, random_state=42)

    mu_x = np.mean(x, axis=0)
    std_x = np.std(x, axis=0)
    mu_y = np.mean(y, axis=0)
    std_y = np.std(y, axis=0)

    package_path  = os.path.dirname(pyur5.__file__)

    metadata_path = os.path.join(package_path, "metadata/metadata_{}_{}.pkl".format(task_typ, use_cos))
    print("metadata_path: ", metadata_path)
    if os.path.exists(metadata_path):
        print("load metadata...")
        metadata = pickle.load(open(metadata_path, "rb"))
        mu_xu = metadata['mu_xu']
        std_xu = metadata['std_xu']
        mu_y = metadata['mu_y']
        std_y = metadata['std_y']
    else:
        print("build metadata...")
        metadata = dict()
        metadata['mu_xu'] = mu_x
        metadata['std_xu'] = std_x
        metadata['mu_y'] = mu_y
        metadata['std_y'] = std_y

        metadata['min_xu'] = xu.min(axis=0)
        metadata['max_xu'] = x.max(axis=0)
        with open(metadata_path, "wb") as f:
            pickle.dump(metadata, f)

    x = normalize(x, mu_x, std_x)
    y = normalize(y, mu_y, std_y)

    x_val = normalize(x_val, mu_x, std_x)
    y_val = normalize(y_val, mu_y, std_y)

    x_tst = normalize(x_tst, mu_x, std_x)
    y_true = normalize(y_true, mu_y, std_y)


    
    return x.astype(np.float32), y.astype(np.float32), x_val.astype(np.float32), y_val.astype(np.float32), x_tst.astype(np.float32), y_true.astype(np.float32)

