import jax.numpy as jnp
import pickle

import numpy as np
import sys

from mbse.models.environment_models.pendulum_swing_up import CustomPendulumEnv
from mbse.utils.replay_buffer import Transition

import os 
import pyur5

package_path = os.path.dirname(pyur5.__file__)

data = dict()
train_horizon = 1 

time_limit = 5
n_envs = 1
seed = 0 
env_kwargs = {}

task_typ = 'sim'
use_cos = True
train_horizon = 1

env = CustomPendulumEnv()#, wrapper_class=wrapper_cls)#, n_envs=n_envs, seed=seed, env_kwargs=env_kwargs)
action_max = env.action_space.high[0]


num_data_points = 100000
n_rollout = 50

obs_vec = np.zeros((num_data_points, env.observation_space.shape[0]))
action_vec = np.zeros((num_data_points, env.action_space.shape[0]))
next_obs_vec = np.zeros((num_data_points, env.observation_space.shape[0]))
reward_vec = np.zeros((num_data_points, 1))
done_vec = np.zeros((num_data_points, 1))
dobs_vec = np.zeros((num_data_points, env.observation_space.shape[0]))

sample_per_rollout = int(num_data_points/n_rollout)
ind = 0
for i in range(n_rollout):
    obs, _ = env.reset_random()
    for j in range(sample_per_rollout):
        action = env.action_space.sample()
        next_obs, reward, done, info, _ = env.step(action)
        obs_vec[ind] = obs
        action_vec[ind] = action
        reward_vec[ind] = reward
        next_obs_vec[ind] = next_obs
        dobs_vec[ind] = next_obs - obs
        done_vec[ind] = done
        obs = next_obs
        
        ind += 1

data = dict()
data['states'] = obs_vec
data['x'] = jnp.concatenate([obs_vec, action_vec], axis=1)
data['y'] = dobs_vec
data['u'] = action_vec
data['states_next'] = next_obs_vec

data['mu_x'] = data['x'].mean(0)
data['mu_y'] = dobs_vec.mean(0)
data['std_x'] = data['x'].std(0)
data['std_y'] = dobs_vec.std(0)

data['transitions'] = Transition(
    obs=obs_vec,
    action=action_vec,
    reward=reward_vec,
    next_obs=next_obs_vec,
    done=done_vec,
)
    

data_path = os.path.join(package_path, "data_pkl/data_{}_{}_{}_{}.pkl".format(task_typ, action_max, train_horizon, use_cos))
with open(data_path, "wb") as f:
    pickle.dump(data, f)


