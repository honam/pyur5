
import pyur5
from pyur5.utils.utils import denormalize, normalize, get_data_jax
import jax.numpy as jnp 
from jax.lax import cond
from trajax.optimizers import CEMHyperparams, ILQRHyperparams, ilqr_with_cem_warmstart, cem, ilqr
import pickle
import math
import jax
import os
import optax

from gym.spaces import Box
import numpy as np
from gym.envs.classic_control.pendulum import angle_normalize
import pandas as pd
import cloudpickle
from mbse.models import *

class EnsembleModel():
    def __init__(self, n_action=2, n_horizon=50, use_cos=True, task_typ='new'):#, path_model, paths_params):
        print("use_cos: ", use_cos)
        self.use_cos = use_cos

        self.target_state = jnp.array([math.pi, 0.0])
        self.cost_weights = jnp.array([10, 1, 0.01])
        
        self.task_typ = task_typ

        package_path = os.path.dirname(pyur5.__file__)
        if task_typ == 'new':
            self.n_action = 2 # number of actions applied (x, y)
            self.n_obs = 3 # the number of axes observed (x, y, z)
            if self.use_cos:
                model_path = os.path.join(package_path, 'model_file', 'new_best_model_frosty-sweep-10.pkl')
                self.x_dim = self.n_obs*2 + 3
            else:
                model_path = os.path.join(package_path, 'model_file', 'new_best_model_cerulean-sweep-4.pkl')
                self.x_dim = self.n_obs*2 + 2

        elif task_typ == 'sim':
            model_path = os.path.join(package_path, 'model_file', 'sim_best_model_cerulean-sweep-1.pkl')
            self.x_dim = 3
            self.n_action = 1
            self.n_obs = 0

        print("model path: ", model_path)
        if os.path.exists(model_path):
            with open(model_path, "rb") as f:
                print("model path: ", model_path)
                self.model = cloudpickle.load(f)
        else:
            self.model = None

        sac_path = os.path.join(package_path, "model_file","sac_policy_{}_{}.pkl".format(self.task_typ, self.use_cos))
        self.sac_path = sac_path

        if os.path.exists(sac_path):
            with open(sac_path, "rb") as f:
                self.sac_policy = cloudpickle.load(f)
        else:
            self.sac_policy = None

        self.cem_params = CEMHyperparams(max_iter=200, sampling_smoothing=0.0, num_samples=200, evolution_smoothing=0.0,
                        elite_portion=0.1)  
        self.ilqr_params = ILQRHyperparams(maxiter=200, make_psd=True, psd_delta=1e-2)

        metadata_path = os.path.join(package_path, 'metadata', "metadata_{}_{}.pkl".format(self.task_typ, self.use_cos))
        
        if os.path.exists(metadata_path):
            with open(metadata_path, "rb") as f:
                self.metadata = pickle.load(f)
            if task_typ == 'new':
                self.obs_min = self.metadata['min_xu'][:-self.n_action]
                self.obs_max = self.metadata['max_xu'][:-self.n_action]
                
                self.action_min = self.metadata['min_xu'][-self.n_action:]
                self.action_max = self.metadata['max_xu'][-self.n_action:]
        else:
            self.metadata = None

        if task_typ == 'sim':
            self.obs_min = np.array([-1.0, -1.0, -8.0])
            self.obs_max = np.array([1.0, 1.0, 8.0]) 

            self.action_min = -2.0
            self.action_max = 2.0

        self.u_dim = self.n_action
        self.n_horizon = n_horizon
            
        if self.metadata is not None:
            print("obs_min", self.obs_min)
            print("obs_max", self.obs_max)
            
            print("action_min", self.action_min)
            print("action_max", self.action_max)
            
            self.obs_space = Box(low=self.obs_min, high=self.obs_max, dtype=np.float32)
            self.action_space = Box(low=self.action_min, high=self.action_max, dtype=np.float32)
            self.rng = jax.random.PRNGKey(seed=0)

    @staticmethod
    @jax.jit
    def normalize(x, u, metadata, n_action=None):
        if u is None:
            inputs = x
            mu_x = metadata['mu_xu'][:-n_action]
            std_x = metadata['std_xu'][:-n_action]
        else:   
            inputs = jnp.concatenate([x, u]).transpose()
            mu_x = metadata['mu_xu']
            std_x = metadata['std_xu']
        print("the shape of inputs for normalization: ", inputs.shape)
        inputs = normalize(inputs, mu_x, std_x)
        return inputs
    
    @staticmethod
    @jax.jit
    def denormalize(out, metadata):    
        out = denormalize(out, metadata['mu_y'], metadata['std_y'])
        return out
    
    def _predict(self, xu):
        
        y_pred = self.model.predict(xu)
        mu, sig = jnp.split(y_pred, 2, axis=-1)
        ds = mu.mean(0)
        return ds
    
    def normalize_and_predict(self, x, u):
        '''
            predict the dx given x and u after normalizing x and u
        '''
        # print("x before normalize: ", x)
        # print("u before normalize: ", u)
        print("metadata: ", self.metadata)
        xu = self.normalize(x, u, self.metadata)
        print("xu after normalize: ", xu)
        dx = self._predict(xu) #self.predict(xu)
        dx = self.denormalize(dx, self.metadata)

        #dx = self.denormalize(dx, self.metadata)
        print("dx value of shape: ", dx.shape)
        return dx
            
    def step(self, x, u):
        '''
            predict the next state given x and u
            
            input:
                x: (x_dim, )
                u: (u_dim, ) rescaled action
            
            output:
                x_next: (x_dim, )
                terminate: bool
                truncate: bool
                output_dict: dict
        '''
        u = u * self.action_max 
        dx = self.normalize_and_predict(x, u)
        x_next = x + dx
        if self.task_typ == 'new':
            if not self.use_cos:
                x_next = x_next.at[0].set(angle_normalize(x_next[0]))
            else:
                theta = jnp.arctan2(x_next[1], x_next[0])
                theta = angle_normalize(theta)
                x_next = x_next.at[0].set(jnp.cos(theta))
                x_next = x_next.at[1].set(jnp.sin(theta))

            # clip position so that it is within the range
                for i in range(self.n_obs):
                    if self.use_cos:
                        pos_idx = 3 + i
                    else:
                        pos_idx = 2 + i
                    x_next = x_next.at[pos_idx].set(jnp.clip(x_next[pos_idx], self.obs_min[pos_idx], self.obs_max[pos_idx]))
        
        print("x_next of shape: ", x_next.shape)
        reward = self._reward_fn(x, u)
        return x_next, reward, False, {}

    def _reward_fn(self, state, action, t=0):
        print("reward function called")
        
        return -self._cost_fn(state, action, t)
    
    def _cost_fn(self, state, action, t): 
        print("cost function called")
        print("state shape: ", state.shape)
        print("action shape: ", action.shape)
        print("state: ", state)
        print("action: ", action)
        assert state.shape == (self.x_dim,) and action.shape == (self.u_dim,)

        if self.use_cos:
            theta = jnp.arctan2(state[1], state[0]).reshape(1, )
            state = jnp.concatenate([theta, state[2:]])
            print("state shape: ", state.shape)
            print("theta: ", theta)

        theta = state[0]
        theta_dot = state[1]
        
        qs = self.cost_weights
        target_state = self.target_state
        
        theta_star = target_state[0]
        theta_dot_star = target_state[1]

        q_theta = qs[0]
        q_theta_dot = qs[1]
        q_u = qs[2]

        dtheta = theta - theta_star
        dtheta_dot = theta_dot - theta_dot_star
        dtheta = angle_normalize(dtheta)
        
        def running_cost(dtheta, dtheta_dot, action):
            return q_theta * jnp.sum(dtheta ** 2) + \
                + q_theta_dot * jnp.sum(dtheta_dot**2) \
                + q_u * jnp.sum(action ** 2)
        
        def terminal_cost(dtheta, dtheta_dot, action):
            return q_theta * jnp.sum(dtheta ** 2) + \
                + q_theta_dot * jnp.sum(dtheta_dot**2)

        return cond(t == self.n_horizon, terminal_cost, running_cost, dtheta, dtheta_dot, action)

    def _dynamics_fn(self, x, u, t):
        '''
            Constrained dynamics function
        '''
        print(x.shape)
        print(u.shape)
        print("x of dynamics_fn: ", x)
        print("u of dynamics_fn: ", u)
        assert x.shape == (self.x_dim,) and u.shape == (self.u_dim,)
        # u = self.action_max * jnp.tanh(u) # hypertangent trick 
        # TODO: Make this flexible so that we pass in values for sim_dynamics
        x_next, _, _, _ = self.step(x, u)
        print("x_next of dynamics_fn: ", x_next)
        return x_next
    
    def _dynamics_fn_unconstrained(self, x, u, t):
        print(x.shape)
        print(u.shape)
        assert x.shape == (self.x_dim,) and u.shape == (self.u_dim,)
        u = self.action_max * jnp.tanh(u) # hypertangent trick 
        x_next, _, _, _ = self.step(x, u)
        print("x_next of dynamics_fn: ", x_next)
        return x_next

    def _dynamics_fn_cond(self, constrained=True):
        if constrained:
            return self._dynamics_fn
        else:
            return self._dynamics_fn_unconstrained
    
    def random_action(self):
        return self.action_space.sample()
    
    def forward_traj(self, x, n_horizon, optimizer='cem_ilqr', sac_policy=None):
        # x_traj = jnp.zeros((n_horizon, x.shape[0]))

        self.n_horizon = n_horizon
        rng = jax.random.PRNGKey(seed=0)
        actor_rng, rng = jax.random.split(rng, 2)
        
        u = jnp.zeros((n_horizon, self.u_dim))

        print(x.shape)
        print(u.shape)

        print("x: ", x)
        
        if optimizer == 'cem':
            out = cem(self._cost_fn, self._dynamics_fn, x, u, control_low=self.action_min,
                              control_high=self.action_max, hyperparams=self.cem_params)

        elif optimizer == 'sac':
            #f = jax.vmap(sac_policy.get_action_for_eval, (None, 0, 0))
            us = []
            xs = []
            cost = 0
            for i in range(self.n_horizon):
                u = sac_policy.get_action_for_eval(x, rng, 0)
                us.append(u)
                xs.append(x)
                x, _, _, _ = self.step(x, u)
                
                cost_single = self._cost_fn(x, u, i)
                
                cost += cost_single
            out = [jnp.array(xs), jnp.array(us), cost]
        else:
            if optimizer == 'cem_ilqr':
                print("cem_ilqr starts!")
                out = ilqr_with_cem_warmstart(self._cost_fn, self._dynamics_fn_unconstrained, x, u, control_low=self.action_min,
                                control_high=self.action_max, ilqr_hyperparams=self.ilqr_params, cem_hyperparams=self.cem_params)
                xs = out[0]
                us = out[1]
                cost = out[2]
                
            elif optimizer == 'ilqr':
                out = ilqr(self._cost_fn, self._dynamics_fn_unconstrained, x, u)
                xs = out[0]
                us = out[1]
                cost = out[2]
                
            us = self.action_max * jnp.tanh(us) #self.action_max * jnp.tanh(us)
                
            out = [xs, us, cost]

        xs = out[0]
        us = out[1]
        cost = out[2]
        return xs, us, cost

    def load_trajs(self):
        actions = np.loadtxt("/home/bizoffermark/workspace/ode/actions.txt", delimiter=',', dtype=np.float32)
        # convert actions to numpy array
        return actions
    
    def forward_traj_and_save(self, x, n_horizon, optimizer='cem_ilqr', sac_policy=None):
        out = self.forward_traj(x, n_horizon, optimizer, sac_policy)
        us = out[1]
        print("us: ", us)
        print("xs: ", out[0])
        import csv
        with open("/home/bizoffermark/workspace/ode/actions.txt", "w") as f:
            writer = csv.writer(f)
            writer.writerows(us)

    def get_sac_action(self, x):
        if self.sac_policy is None:
            print("load sac policy")
            self.sac_policy = cloudpickle.load(open(self.sac_path, "rb"))
        # print("sac policy loaded")
        # print("x shape: ", x.shape)
        with open("/home/bizoffermark/workspace/ode/recording.txt", "a") as f:
            f.write(x.__str__() + "\n")
        actor_rng, self.rng = jax.random.split(self.rng, 2)

        u = self.sac_policy.get_action_for_eval(x, self.rng, 0)

        # print("u: ", u)
        return u
