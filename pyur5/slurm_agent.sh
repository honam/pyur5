#!/bin/bash

#SBATCH --job-name traj # Name for your job
#SBATCH --ntasks 4              # Number of (cpu) tasks
#SBATCH --time 15               # Runtime in minutes.
#SBATCH --mem=2000             # Reserve 1 GB RAM for the job
eval "$(conda shell.bash hook)"
conda activate

wandb agent bizoffermark/ur5_test_plot/xf9lr8rt