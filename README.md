# PyUR5 
PyUR5 contains a simple example of using pybind to call python functions in 

## Running the file

```bash
python3 run.py --cmake
python3 run.py --build
```

If you want to run it this way, make sure to change the corresponding directory in run.py

## Important things to note
Make sure to run cmake ALWAYS with -DPython_EXECUTABLE=<PYTHON INTERPRETER PATH> if you use conda

## Files

checkpoints contain all training results from traing the model (literally everything!)

data contain recording data used for training the network

metadata contain all metadata regarding training data (e.g. min, max, std, mean)


