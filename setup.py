from setuptools import setup, find_packages

required = [
    'tensorflow',
    'tensorflow_datasets',
    'mbse @ git+https://github.com/bizoffermark/mbse.git'
]

# required = []

extras = {'dev': ['seaborn', 'control>=0.9.2']}
setup(
    name='pyur5',
    version='0.0.1',
    packages=find_packages(),
    python_requires='>=3.8',
    # package_dir = {"": ""},
    package_data={'pyur5.metadata': ['*.pkl'],
                 'pyur5.model_file': ['*.pkl']},
    include_package_data=True,
    install_requires=required,
    extras_require=extras
    )